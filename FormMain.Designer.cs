﻿namespace KSReadinessTool
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpStatus = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTransferStatus = new System.Windows.Forms.Label();
            this.progressBarDataTransfer = new System.Windows.Forms.ProgressBar();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lblProgress = new System.Windows.Forms.Label();
            this.lblSpeedRate = new System.Windows.Forms.Label();
            this.txtLogSummary = new System.Windows.Forms.TextBox();
            this.btnRunNow = new System.Windows.Forms.Button();
            this.btnViewResults = new System.Windows.Forms.Button();
            this.chkBoxAutoRun = new System.Windows.Forms.CheckBox();
            this.numericNetworkTestSchedule = new System.Windows.Forms.NumericUpDown();
            this.lblMinutes = new System.Windows.Forms.Label();
            this.bgwCopierProgress = new System.ComponentModel.BackgroundWorker();
            this.timerResetTransferRatePerSec = new System.Windows.Forms.Timer(this.components);
            this.timerScheduleNetworkTest = new System.Windows.Forms.Timer(this.components);
            this.timerUpdateTransferStatusText = new System.Windows.Forms.Timer(this.components);
            this.grpSummary = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblAverageBandwidth = new System.Windows.Forms.Label();
            this.lblNumberOfTests = new System.Windows.Forms.Label();
            this.txtAvgBandwidth = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.grpStatus.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericNetworkTestSchedule)).BeginInit();
            this.grpSummary.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpStatus
            // 
            this.grpStatus.Controls.Add(this.tableLayoutPanel4);
            this.grpStatus.Controls.Add(this.txtLogSummary);
            this.grpStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.grpStatus.Location = new System.Drawing.Point(23, 148);
            this.grpStatus.Name = "grpStatus";
            this.grpStatus.Size = new System.Drawing.Size(558, 230);
            this.grpStatus.TabIndex = 2;
            this.grpStatus.TabStop = false;
            this.grpStatus.Text = "Status";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.lblTransferStatus, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.progressBarDataTransfer, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(34, 19);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(479, 71);
            this.tableLayoutPanel4.TabIndex = 4;
            // 
            // lblTransferStatus
            // 
            this.lblTransferStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTransferStatus.AutoSize = true;
            this.lblTransferStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTransferStatus.Location = new System.Drawing.Point(3, 0);
            this.lblTransferStatus.Name = "lblTransferStatus";
            this.lblTransferStatus.Size = new System.Drawing.Size(473, 21);
            this.lblTransferStatus.TabIndex = 0;
            this.lblTransferStatus.Text = "No tests scheduled, standing by";
            this.lblTransferStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBarDataTransfer
            // 
            this.progressBarDataTransfer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarDataTransfer.Location = new System.Drawing.Point(3, 24);
            this.progressBarDataTransfer.Name = "progressBarDataTransfer";
            this.progressBarDataTransfer.Size = new System.Drawing.Size(473, 18);
            this.progressBarDataTransfer.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.lblProgress, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblSpeedRate, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 48);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(473, 20);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // lblProgress
            // 
            this.lblProgress.AutoSize = true;
            this.lblProgress.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProgress.Location = new System.Drawing.Point(3, 0);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(117, 20);
            this.lblProgress.TabIndex = 2;
            this.lblProgress.Text = "Data Transfer Statistics";
            // 
            // lblSpeedRate
            // 
            this.lblSpeedRate.AutoSize = true;
            this.lblSpeedRate.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblSpeedRate.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpeedRate.Location = new System.Drawing.Point(410, 0);
            this.lblSpeedRate.Name = "lblSpeedRate";
            this.lblSpeedRate.Size = new System.Drawing.Size(60, 20);
            this.lblSpeedRate.TabIndex = 3;
            this.lblSpeedRate.Text = "Speed Rate";
            // 
            // txtLogSummary
            // 
            this.txtLogSummary.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtLogSummary.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtLogSummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLogSummary.Location = new System.Drawing.Point(6, 96);
            this.txtLogSummary.Multiline = true;
            this.txtLogSummary.Name = "txtLogSummary";
            this.txtLogSummary.ReadOnly = true;
            this.txtLogSummary.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLogSummary.Size = new System.Drawing.Size(546, 128);
            this.txtLogSummary.TabIndex = 4;
            // 
            // btnRunNow
            // 
            this.btnRunNow.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRunNow.Font = new System.Drawing.Font("", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRunNow.Location = new System.Drawing.Point(3, 35);
            this.btnRunNow.Name = "btnRunNow";
            this.btnRunNow.Size = new System.Drawing.Size(126, 26);
            this.btnRunNow.TabIndex = 1;
            this.btnRunNow.Text = "Run Test Now";
            this.btnRunNow.UseVisualStyleBackColor = true;
            this.btnRunNow.Click += new System.EventHandler(this.BtnRunNow_Click);
            // 
            // btnViewResults
            // 
            this.btnViewResults.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btnViewResults.Font = new System.Drawing.Font("", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewResults.Location = new System.Drawing.Point(215, 35);
            this.btnViewResults.Name = "btnViewResults";
            this.btnViewResults.Size = new System.Drawing.Size(126, 26);
            this.btnViewResults.TabIndex = 2;
            this.btnViewResults.Text = "View Results";
            this.btnViewResults.UseVisualStyleBackColor = true;
            this.btnViewResults.Click += new System.EventHandler(this.BtnViewResults_Click);
            // 
            // chkBoxAutoRun
            // 
            this.chkBoxAutoRun.AutoSize = true;
            this.chkBoxAutoRun.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkBoxAutoRun.Font = new System.Drawing.Font("", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxAutoRun.Location = new System.Drawing.Point(3, 3);
            this.chkBoxAutoRun.Name = "chkBoxAutoRun";
            this.chkBoxAutoRun.Size = new System.Drawing.Size(161, 26);
            this.chkBoxAutoRun.TabIndex = 1;
            this.chkBoxAutoRun.Text = "Automatically run test every";
            this.chkBoxAutoRun.UseVisualStyleBackColor = true;
            this.chkBoxAutoRun.CheckedChanged += new System.EventHandler(this.ChkBoxAutoRun_CheckedChanged);
            // 
            // numericNetworkTestSchedule
            // 
            this.numericNetworkTestSchedule.Enabled = false;
            this.numericNetworkTestSchedule.Location = new System.Drawing.Point(3, 3);
            this.numericNetworkTestSchedule.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericNetworkTestSchedule.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericNetworkTestSchedule.Name = "numericNetworkTestSchedule";
            this.numericNetworkTestSchedule.Size = new System.Drawing.Size(66, 20);
            this.numericNetworkTestSchedule.TabIndex = 0;
            this.numericNetworkTestSchedule.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericNetworkTestSchedule.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericNetworkTestSchedule.ValueChanged += new System.EventHandler(this.NumericNetworkTestSchedule_ValueChanged);
            this.numericNetworkTestSchedule.KeyUp += new System.Windows.Forms.KeyEventHandler(this.NumericNetworkTestSchedule_KeyUp);
            // 
            // lblMinutes
            // 
            this.lblMinutes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblMinutes.AutoSize = true;
            this.lblMinutes.Font = new System.Drawing.Font("", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinutes.Location = new System.Drawing.Point(80, 0);
            this.lblMinutes.Name = "lblMinutes";
            this.lblMinutes.Size = new System.Drawing.Size(44, 26);
            this.lblMinutes.TabIndex = 1;
            this.lblMinutes.Text = "Minutes";
            this.lblMinutes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bgwCopierProgress
            // 
            this.bgwCopierProgress.WorkerReportsProgress = true;
            this.bgwCopierProgress.WorkerSupportsCancellation = true;
            this.bgwCopierProgress.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BgwCopierProgress_DoWork);
            this.bgwCopierProgress.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BgwCopierProgress_ProgressChanged);
            this.bgwCopierProgress.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BgwCopierProgress_RunWorkerCompleted);
            // 
            // timerResetTransferRatePerSec
            // 
            this.timerResetTransferRatePerSec.Interval = 1000;
            this.timerResetTransferRatePerSec.Tick += new System.EventHandler(this.TimerSpeed_Tick);
            // 
            // timerScheduleNetworkTest
            // 
            this.timerScheduleNetworkTest.Tick += new System.EventHandler(this.TimerStartNetworkTest_Tick);
            // 
            // timerUpdateTransferStatusText
            // 
            this.timerUpdateTransferStatusText.Enabled = true;
            this.timerUpdateTransferStatusText.Interval = 1000;
            this.timerUpdateTransferStatusText.Tick += new System.EventHandler(this.TimerUpdateTransferStatusText_Tick);
            // 
            // grpSummary
            // 
            this.grpSummary.Controls.Add(this.tableLayoutPanel1);
            this.grpSummary.Location = new System.Drawing.Point(23, 91);
            this.grpSummary.Name = "grpSummary";
            this.grpSummary.Size = new System.Drawing.Size(558, 51);
            this.grpSummary.TabIndex = 1;
            this.grpSummary.TabStop = false;
            this.grpSummary.Text = "Summary";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this.lblAverageBandwidth, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNumberOfTests, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtAvgBandwidth, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(30, 15);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(483, 30);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblAverageBandwidth
            // 
            this.lblAverageBandwidth.AutoSize = true;
            this.lblAverageBandwidth.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblAverageBandwidth.Location = new System.Drawing.Point(38, 0);
            this.lblAverageBandwidth.Name = "lblAverageBandwidth";
            this.lblAverageBandwidth.Size = new System.Drawing.Size(103, 30);
            this.lblAverageBandwidth.TabIndex = 0;
            this.lblAverageBandwidth.Text = "Average Bandwidth:";
            this.lblAverageBandwidth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNumberOfTests
            // 
            this.lblNumberOfTests.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNumberOfTests.AutoSize = true;
            this.lblNumberOfTests.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lblNumberOfTests.Location = new System.Drawing.Point(379, 0);
            this.lblNumberOfTests.Name = "lblNumberOfTests";
            this.lblNumberOfTests.Size = new System.Drawing.Size(101, 30);
            this.lblNumberOfTests.TabIndex = 3;
            this.lblNumberOfTests.Text = "No Tests Performed";
            this.lblNumberOfTests.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtAvgBandwidth
            // 
            this.txtAvgBandwidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAvgBandwidth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAvgBandwidth.Location = new System.Drawing.Point(147, 3);
            this.txtAvgBandwidth.Name = "txtAvgBandwidth";
            this.txtAvgBandwidth.ReadOnly = true;
            this.txtAvgBandwidth.Size = new System.Drawing.Size(187, 20);
            this.txtAvgBandwidth.TabIndex = 1;
            this.txtAvgBandwidth.Text = "---";
            this.txtAvgBandwidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.btnRunNow, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.chkBoxAutoRun, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnViewResults, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(124, 18);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(424, 64);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.5F));
            this.tableLayoutPanel3.Controls.Add(this.numericNetworkTestSchedule, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblMinutes, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(215, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(206, 26);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 390);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.grpSummary);
            this.Controls.Add(this.grpStatus);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KS Readiness Tool";
            this.grpStatus.ResumeLayout(false);
            this.grpStatus.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericNetworkTestSchedule)).EndInit();
            this.grpSummary.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpStatus;
        private System.Windows.Forms.ProgressBar progressBarDataTransfer;
        private System.Windows.Forms.Button btnRunNow;
        private System.Windows.Forms.Button btnViewResults;
        private System.Windows.Forms.CheckBox chkBoxAutoRun;
        private System.Windows.Forms.NumericUpDown numericNetworkTestSchedule;
        private System.Windows.Forms.Label lblMinutes;
        private System.Windows.Forms.Label lblProgress;
        private System.Windows.Forms.Label lblTransferStatus;
        private System.ComponentModel.BackgroundWorker bgwCopierProgress;
        private System.Windows.Forms.Timer timerResetTransferRatePerSec;
        private System.Windows.Forms.Label lblSpeedRate;
        private System.Windows.Forms.Timer timerScheduleNetworkTest;
        private System.Windows.Forms.Timer timerUpdateTransferStatusText;
        private System.Windows.Forms.TextBox txtLogSummary;
        private System.Windows.Forms.GroupBox grpSummary;
        private System.Windows.Forms.Label lblNumberOfTests;
        private System.Windows.Forms.TextBox txtAvgBandwidth;
        private System.Windows.Forms.Label lblAverageBandwidth;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
    }
}

