using KSReadinessTool.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace KSReadinessTool
{
    public partial class FormMain : Form
    {
        #region Variables

        /// <summary>
        /// The next schedule test time
        /// </summary>
        private DateTime? _nextScheduleTestTime;

        /// <summary>
        /// The total content size in text format
        /// </summary>
        private string _totalContentSizeString;

        /// <summary>
        /// variable to hold the running data transfer test result
        /// </summary>
        private TestResult _testResult;

        /// <summary>
        /// List of executed Test Results
        /// </summary>
        private readonly List<TestResult> _testResults = new List<TestResult>();

        /// <summary>
        /// variable to hold the status if the content transfer completed or not to be used in schedule tests timer
        /// </summary>
        private bool _isTransferCompleted;

        #endregion

        #region Custom Methods

        /// <summary>
        /// Starts the connection test.
        /// </summary>
        private void StartConnectionTest()
        {
            timerResetTransferRatePerSec.Enabled = true;
            btnRunNow.Enabled = false;
            _isTransferCompleted = false;

            _testResult = new TestResult();

            bgwCopierProgress.RunWorkerAsync();
        }

        /// <summary>
        /// Resets the timer start network test.
        /// </summary>
        private void ResetTimerStartNetworkTest()
        {
            numericNetworkTestSchedule.Enabled = chkBoxAutoRun.Checked;

            if (chkBoxAutoRun.Checked)
            {
                timerScheduleNetworkTest.Interval = (int)numericNetworkTestSchedule.Value * 60 * 1000;
                _nextScheduleTestTime = DateTime.Now.AddMinutes((int)numericNetworkTestSchedule.Value);
                timerScheduleNetworkTest.Enabled = true;
            }
            else
            {
                timerScheduleNetworkTest.Enabled = false;
                _nextScheduleTestTime = null;
            }
        }

        /// <summary>
        /// Logs the message to the form log textbox control
        /// </summary>
        /// <param name="message">The message.</param>
        private void LogMessage(string message)
        {
            var logMessage = $@"{DateTime.Now.Year}{DateTime.Now.Month:D2}{DateTime.Now.Day:D2}_{DateTime.Now.Hour:D2}{DateTime.Now.Minute:D2}{DateTime.Now.Second:D2}, {message}";
            if (string.IsNullOrEmpty(txtLogSummary.Text))
            {
                txtLogSummary.AppendText(logMessage);
            }
            else
            {
                txtLogSummary.AppendText(Environment.NewLine);
                txtLogSummary.AppendText(logMessage);
            }
        }

        #endregion

        #region Form Events
        /// <summary>
        /// Initializes a new instance of the <see cref="FormMain"/> class.
        /// </summary>
        public FormMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the Click event of the BtnRunNow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void BtnRunNow_Click(object sender, EventArgs e)
        {
            StartConnectionTest();
        }

        /// <summary>
        /// Handles the DoWork event of the BgwCopierProgress control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs" /> instance containing the event data.</param>
        private void BgwCopierProgress_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = (BackgroundWorker)sender;

            if (worker == null)
            {
                return;
            }

            if (worker.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            _testResult.Status = TestStatus.Starting;
            worker.ReportProgress((int)_testResult.Status);

            ResetTimerStartNetworkTest();

            using var connection = new AzNetworkConnection();

            try
            {
                _testResult.Status = TestStatus.TestingConnectivityToDestination;
                worker.ReportProgress((int)_testResult.Status);

                var connectionCheck = connection.CheckSmbAvailability();

                if (!connectionCheck)
                {
                    Utilities.Instance.WriteToLog("Could not connect to the remote location", LogMessageType.Error);

                    _testResult.Status = TestStatus.ErrorOccured;
                    worker.ReportProgress((int)_testResult.Status);

                    worker.CancelAsync();

                    return;
                }

                _testResult.Status = TestStatus.TestingConnectivitySucceeded;
                worker.ReportProgress((int)_testResult.Status);

                _testResult.Status = TestStatus.TransferInProgress;
                worker.ReportProgress((int)_testResult.Status);
                var destinationFileName =
                    $"{AppConfigurations.Instance.AzureFileShareUncPath}\\{DateTime.Now.Year}_{DateTime.Now.Month:D2}_{DateTime.Now.Day:D2}_{DateTime.Now.Minute:D2}_{DateTime.Now.Second:D2}_Large.test";

                var destinationFile = new FileInfo(destinationFileName);

                var buffer = new byte[AppConfigurations.Instance.BufferSizeInBytes];


                var file = new FileInfo(AppConfigurations.Instance.LargeTestFileLocation);

                _testResult.TotalContentSize = file.Length;
                _totalContentSizeString = Utilities.Instance.FormatByteSize(_testResult.TotalContentSize);

                _testResult.FileName = destinationFileName;
                _testResult.FileSize = file.Length;
                _testResult.StartTime = DateTime.Now;

                using (FileStream fsRead = file.Open(FileMode.Open, FileAccess.Read))
                {
                    FileStream fsWrite = destinationFile.Open(FileMode.OpenOrCreate, FileAccess.ReadWrite);

                    _testResult.ProcessedContentSize = 0;

                    int bytesRead;

                    while ((bytesRead = fsRead.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        fsWrite.Write(buffer, 0, bytesRead);

                        _testResult.RecentTransferRatePerSecond += bytesRead;

                        _testResult.ProcessedContentSize += bytesRead;
                        worker.ReportProgress((int)(_testResult.ProcessedContentSize / (_testResult.TotalContentSize / 100)));
                    }

                    _testResult.Status = TestStatus.TransferCompleted;
                    worker.ReportProgress((int)_testResult.Status);

                    _isTransferCompleted = true;


                }

                // File.SetAttributes(destinationFileName, FileAttributes.Normal);

                //_testResult.Status = TestStatus.TestTransferCompletedSuccessfully;
                //worker.ReportProgress((int)_testResult.Status);
            }
            catch (Exception exception)
            {
                connection.Dispose();
                Utilities.Instance.WriteToLog(exception.ToString(), LogMessageType.Error);
                LogMessage($@"Error occured. Please check the log file for more details {exception}");
            }
        }

        /// <summary>
        /// Handles the RunWorkerCompleted event of the BgwCopierProgress control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.RunWorkerCompletedEventArgs" /> instance containing the event data.</param>
        private void BgwCopierProgress_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timerResetTransferRatePerSec.Enabled = false;

            _testResult.EndTime = DateTime.Now;

            if (e.Error != null || _testResult.Status == TestStatus.ErrorOccured)
            {
                progressBarDataTransfer.Value = 0;
                if (e.Error != null)
                {
                    Utilities.Instance.WriteToLog(e.Error.ToString(), LogMessageType.Error);
                }

                return;
            }

            _testResult.Status = TestStatus.TestTransferCompletedSuccessfully;

            progressBarDataTransfer.Value = 100;
            btnRunNow.Enabled = true;

            _testResults.Add(_testResult);

            TestResults.Instance.WriteResult(_testResult);

            lblNumberOfTests.Text = $@"{_testResults.Count} Test Performed";
            txtAvgBandwidth.Text = _testResults.Count == 1 ? $@"{Utilities.Instance.FormatByteSizeToMb(_testResult.AverageTransferRatePerSec)} MBps  -   {(Utilities.Instance.FormatByteSizeToMb(_testResult.AverageTransferRatePerSec) * 8)} Mbps" : $@"{Utilities.Instance.FormatByteSizeToMb(_testResults.Average(r => r.AverageTransferRatePerSec))} MBps  -   {(Utilities.Instance.FormatByteSizeToMb(_testResults.Average(r => r.AverageTransferRatePerSec)) * 8)} Mbps";

            lblSpeedRate.Text =
                $@"{Utilities.Instance.FormatByteSizeToMb(_testResult.AverageTransferRatePerSec)} MBps  -   {(Utilities.Instance.FormatByteSizeToMb(_testResult.AverageTransferRatePerSec) * 8)} Mbps";
        }

        /// <summary>
        /// Handles the ProgressChanged event of the BgwCopierProgress control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.ProgressChangedEventArgs" /> instance containing the event data.</param>
        private void BgwCopierProgress_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

            if (e.ProgressPercentage < 0)
            {
                switch ((TestStatus)Enum.ToObject(typeof(TestStatus), e.ProgressPercentage))
                {
                    case TestStatus.Starting:
                        var publicIp = Utilities.Instance.GetPublicIpAddress();
                        LogMessage(publicIp == ""
                            ? $@"Could not retrieve the Public IP"
                            : $@"Public IP Address: {publicIp}");

                        LogMessage("Starting file transfer...");

                        break;
                    case TestStatus.TestingConnectivityToDestination:
                        LogMessage("Testing the connectivity to the remote destination");
                        break;
                    case TestStatus.TestingConnectivitySucceeded:
                        LogMessage("Testing the connectivity to the remote destination completed successfully");
                        break;
                    case TestStatus.TransferInProgress:
                        LogMessage("Test File transfer is In Progress");
                        break;
                    case TestStatus.ClosingConnection:
                        LogMessage("Closing the connection to the remote destination");
                        break;
                    case TestStatus.ErrorOccured:
                        LogMessage("Error occured. Please check log files for more details");
                        break;
                }
                return;
            }

            lblProgress.Text =
                $@"{Utilities.Instance.FormatByteSize(_testResult.ProcessedContentSize)} / {_totalContentSizeString}   {e.ProgressPercentage}%";

            if (e.ProgressPercentage == 100)
            {
                LogMessage("Test File transfer completed successfully");
            }

            progressBarDataTransfer.Value = e.ProgressPercentage;
        }

        /// <summary>
        /// Handles the Tick event of the TimerSpeed control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void TimerSpeed_Tick(object sender, EventArgs e)
        {
            if (!_isTransferCompleted)
            {

                lblSpeedRate.Text = $@"{Utilities.Instance.FormatByteSize(_testResult.RecentTransferRatePerSecond)}ps - {Utilities.Instance.FormatByteSize(_testResult.RecentTransferRatePerSecond * 8).Replace('B', 'b')}ps";

                if (_testResult.TransferRatesPerSec == null)
                {
                    _testResult.TransferRatesPerSec = new List<int>();
                }

                _testResult.TransferRatesPerSec.Add(_testResult.RecentTransferRatePerSecond);

                _testResult.RecentTransferRatePerSecond = 0;
            }
            else
            {

                lblSpeedRate.Text =
                $@"{Utilities.Instance.FormatByteSize(_testResult.AverageTransferRatePerSec).Replace("MB", "Mb")}ps";


            }
        }

        /// <summary>
        /// Handles the Tick event of the TimerStartNetworkTest control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void TimerStartNetworkTest_Tick(object sender, EventArgs e)
        {
            _nextScheduleTestTime = DateTime.Now.AddMinutes((int)numericNetworkTestSchedule.Value);

            if (bgwCopierProgress.IsBusy)
            {
                return;
            }

            StartConnectionTest();
        }

        /// <summary>
        /// Handles the CheckedChanged event of the ChkBoxAutoRun control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void ChkBoxAutoRun_CheckedChanged(object sender, EventArgs e)
        {
            ResetTimerStartNetworkTest();
        }

        /// <summary>
        /// Handles the Click event of the BtnViewResults control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void BtnViewResults_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", TestResults.Instance.TestResultsDirectoryPath);
        }

        /// <summary>
        /// Handles the Tick event of the TimerUpdateTransferStatusText control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void TimerUpdateTransferStatusText_Tick(object sender, EventArgs e)
        {
            // Test file transfer in process
            if (bgwCopierProgress.IsBusy)
            {
                lblTransferStatus.Text = @"Test file transfer in process";
                return;
            }

            // Test file transfer complete
            if (_testResult != null && (DateTimeOffset.Now.Subtract(_testResult.EndTime).TotalSeconds <= 10))
            {
                btnRunNow.Enabled = true;

                switch (_testResult.Status)
                {
                    case TestStatus.TestTransferCompletedSuccessfully:
                        lblTransferStatus.Text = @"Test file transfer completed successfully";
                        break;
                    case TestStatus.ErrorOccured:
                        lblTransferStatus.Text = @"Error occured";
                        lblProgress.Text = $@"Data Transfer Statistics";
                        progressBarDataTransfer.Value = 0;
                        break;
                }

                return;
            }

            // No tests scheduled, standing by.
            if (!bgwCopierProgress.IsBusy && !chkBoxAutoRun.Checked)
            {
                lblTransferStatus.Text = @"No tests scheduled, standing by";
                progressBarDataTransfer.Value = 0;
                btnRunNow.Enabled = true;
                return;
            }

            // X minutes until next automatic test
            if (!bgwCopierProgress.IsBusy && chkBoxAutoRun.Checked && _nextScheduleTestTime.HasValue)
            {
                btnRunNow.Enabled = true;
                progressBarDataTransfer.Value = 0;

                var remainingTime = (int)_nextScheduleTestTime.Value.Subtract(DateTime.Now).TotalSeconds;

                lblTransferStatus.Text = remainingTime < 60
                    ? $@"{remainingTime:D2} seconds until next automatic test"
                    : $@"{(remainingTime / 60):D2} minutes until next automatic test";

            }

        }

        /// <summary>
        /// Handles the KeyUp event of the NumericNetworkTestSchedule control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs" /> instance containing the event data.</param>
        private void NumericNetworkTestSchedule_KeyUp(object sender, KeyEventArgs e)
        {
            ResetTimerStartNetworkTest();
        }

        /// <summary>
        /// Handles the ValueChanged event of the NumericNetworkTestSchedule control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
        private void NumericNetworkTestSchedule_ValueChanged(object sender, EventArgs e)
        {
            ResetTimerStartNetworkTest();
        }

        #endregion

    }
}

