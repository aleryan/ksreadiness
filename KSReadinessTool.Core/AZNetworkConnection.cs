using System;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;

namespace KSReadinessTool.Core
{
	/// <summary>
	/// A class to manage the connectivity to a network resource 
	/// </summary>
	/// <seealso cref="System.IDisposable" />
	public class AzNetworkConnection : IDisposable
	{
		/// <summary>
		/// The network name
		/// </summary>
		private readonly string _networkName;

		/// <summary>
		/// The credentials used to connect the remote resource
		/// </summary>
		private readonly NetworkCredential _credentials;


		/// <summary>
		/// Initialize the connection with the config values coming from the App.config file
		/// </summary>
		public AzNetworkConnection()
		{
			if (string.IsNullOrEmpty(AppConfigurations.Instance.AzureFileShareUncPath)
				|| string.IsNullOrEmpty(AppConfigurations.Instance.AzureFileShareUserName)
				|| string.IsNullOrEmpty(AppConfigurations.Instance.AzureFileSharePassword)
				)
			{
				Utilities.Instance.WriteToLog("Missing application config values.", LogMessageType.Warning);
			}

			_networkName = AppConfigurations.Instance.AzureFileShareUncPath;
			_credentials = new NetworkCredential(AppConfigurations.Instance.AzureFileShareUserName,
				AppConfigurations.Instance.AzureFileSharePassword, string.Empty);
		}

		/// <summary>
		/// Initiates a new connection to Azure File Share.
		/// </summary>
		/// <returns></returns>
		private int Connect()
		{
			var netResource = new NetResource()
			{
				Scope = ResourceScope.GlobalNetwork,
				ResourceType = ResourceType.Disk,
				DisplayType = ResourceDisplayType.Share,
				RemoteName = _networkName
			};

			var userName = string.IsNullOrEmpty(_credentials.Domain)
				? _credentials.UserName
				: $@"{_credentials.Domain}\{_credentials.UserName}";

			var result = WNetAddConnection2(
				netResource,
				_credentials.Password,
				userName,
				0);
			return result;
		}

		/// <summary>
		/// Checks the SMB availability.
		/// </summary>
		/// <returns></returns>
		/// <exception cref="Exception">@"Could not connect to the specified remote connection. Result returned error code {result}")</exception>
		public bool CheckSmbAvailability()
		{
			try
			{
				var result = Connect();
				if (result != 0 && result != 1219)
				{
					throw new Exception(
						$@"Could not connect to the specified remote connection. Result returned error code {result}. For more details please check the error description https://docs.microsoft.com/en-us/windows/desktop/Debug/system-error-codes");
				}
				File.Copy(AppConfigurations.Instance.SmallTestFileLocation,
					$@"{AppConfigurations.Instance.AzureFileShareUncPath}\\small.test", true);

				var latestTestResultFile = TestResults.Instance.GetRecentTestResultFile();
				if (latestTestResultFile != null)
				{
					File.Copy(latestTestResultFile.FullName, $@"{AppConfigurations.Instance.AzureFileShareUncPath}\\{latestTestResultFile.Name}", true);
				}

				return true;
			}
			catch (Exception exception)
			{
				Utilities.Instance.WriteToLog(exception.ToString(), LogMessageType.Error);
				return false;
			}
		}

		/// <summary>
		/// Finalizes an instance of the <see cref="AzNetworkConnection"/> class.
		/// </summary>
		~AzNetworkConnection()
		{
			Dispose(false);
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Releases unmanaged and - optionally - managed resources.
		/// </summary>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected virtual void Dispose(bool disposing)
		{
			WNetCancelConnection2(_networkName, 0, true);
		}

		[DllImport("mpr.dll")]
		private static extern int WNetAddConnection2(NetResource netResource, string password, string username, int flags);

		[DllImport("mpr.dll")]
		private static extern int WNetCancelConnection2(string name, int flags, bool force);
	}

}
