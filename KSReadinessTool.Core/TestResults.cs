using System;
using System.IO;
using System.Linq;


namespace KSReadinessTool.Core
{
    /// <summary>
    /// Class to manage the functions of Test Results
    /// </summary>
    public class TestResults
    {

		/// <summary>
		/// CSV header fields
		/// </summary>
        private readonly string _testResultFileHeader = $@"Test Time,File Name,File Size (MB),Network Transfer Time (Seconds)";

		/// <summary>
		/// An instance of TestResults calss
		/// </summary>
		public static readonly TestResults Instance = new TestResults();

        /// <summary>Gets the test results directory path.</summary>
        /// <value>The test results directory path.</value>
        public string TestResultsDirectoryPath
        {
            get
            {
                var testResultDirectoryPath = $@"{Directory.GetCurrentDirectory()}\TestResults";
                if (Directory.Exists(testResultDirectoryPath)) return testResultDirectoryPath;
                var directory = Directory.CreateDirectory(testResultDirectoryPath);
                return directory.FullName;
            }
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="TestResults"/> class from being created.
        /// </summary>
        private TestResults()
        {
        }

        /// <summary>Writes the result.</summary>
        /// <param name="testResult">The test result.</param>
        public void WriteResult(TestResult testResult)
        {
            var publicIp = Utilities.Instance.GetPublicIpAddress().Replace('.', '-');
            var fileName =
                $@"{TestResultsDirectoryPath}\\{DateTime.Now.Year}{DateTime.Now.Month:D2}{DateTime.Now.Day:D2}_{publicIp}_{Environment.MachineName}_KSR.csv";

            var testResultValue =
                $@"{testResult.TestTime},{testResult.FileName},{Utilities.Instance.FormatByteSizeToMb(testResult.FileSize)},{testResult.TotalTransferTime}{Environment.NewLine}";

            if (!File.Exists(fileName))
            {
                File.AppendAllText(fileName, _testResultFileHeader);
                File.AppendAllText(fileName, Environment.NewLine);
                File.AppendAllText(fileName, testResultValue);

                return;
            }

            var fileContent = File.ReadAllLines(fileName);
            if (fileContent.Length > 0 &&
                _testResultFileHeader.ToLower().Equals(fileContent[0].ToLower().Replace("\n", "")))
            {
                File.AppendAllText(fileName, testResultValue);
            }
            else
            {
                File.AppendAllText(fileName, _testResultFileHeader);
                File.AppendAllText(fileName, Environment.NewLine);
                File.AppendAllText(fileName, testResultValue);
            }
        }

		/// <summary>
		/// Get most recet Test Result file
		/// </summary>
		/// <returns></returns>
        public FileInfo GetRecentTestResultFile()
        {
           var directory = new DirectoryInfo(TestResultsDirectoryPath);
            return directory.GetFiles()
                .OrderByDescending(f => f.LastWriteTime)
                .FirstOrDefault();
        }

    }
}


