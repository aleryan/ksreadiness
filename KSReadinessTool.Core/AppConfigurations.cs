using System.Configuration;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Runtime.Remoting.Messaging;

namespace KSReadinessTool.Core
{
    /// <summary>
    /// Class to manage the application custom configurations 
    /// </summary>
    public class AppConfigurations
    {
        /// <summary>
        /// A Singleton Instance for the class
        /// </summary>
        public static readonly AppConfigurations Instance = new AppConfigurations();


        /// <summary>
        /// A reference to the app.config Configurations
        /// </summary>
        private readonly Configuration _config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

        /// <summary>
        /// Gets the azure file share unc path.
        /// </summary>
        /// <value>
        /// The azure file share unc path.
        /// </value>
        public string AzureFileShareUncPath => _config.AppSettings.Settings["AzureFileShareUrl"] != null ?
            _config.AppSettings.Settings["AzureFileShareUrl"].Value : string.Empty;

        /// <summary>
        /// Gets the name of the azure file share user.
        /// </summary>
        /// <value>
        /// The name of the azure file share user.
        /// </value>
        public string AzureFileShareUserName => _config.AppSettings.Settings["AzureFileShareUserName"] != null
            ? _config.AppSettings.Settings["AzureFileShareUserName"].Value
            : string.Empty;

        /// <summary>
        /// Gets the azure file share password.
        /// </summary>
        /// <value>
        /// The azure file share password.
        /// </value>
        public string AzureFileSharePassword => _config.AppSettings.Settings["AzureFileSharePassword"] != null
            ? _config.AppSettings.Settings["AzureFileSharePassword"].Value
            : string.Empty;

        /// <summary>
        /// Gets the small test file location.
        /// </summary>
        /// <value>
        /// The small test file location.
        /// </value>
        public string SmallTestFileLocation
        {
            get
            {
                if (_config.AppSettings.Settings["SmallTestFileLocation"] != null
                    && !string.IsNullOrWhiteSpace(_config.AppSettings.Settings["SmallTestFileLocation"].Value))

                    return _config.AppSettings.Settings["SmallTestFileLocation"].Value;

                if (Directory.Exists(DefaultTestFilesDirectoryLocation))
                {
                    // If the test file exists return the full file path, otherwise generate a dump file
                    return File.Exists($@"{DefaultTestFilesDirectoryLocation}\\Small.test")
                        ? $@"{DefaultTestFilesDirectoryLocation}\\Small.test"
                        : Utilities.Instance.GenerateDumpFile($@"{DefaultTestFilesDirectoryLocation}\\Small.test",
                            SmallTestFileSizeInBytes).FullName;
                }

                Directory.CreateDirectory(DefaultTestFilesDirectoryLocation);
                return Utilities.Instance.GenerateDumpFile($@"{DefaultTestFilesDirectoryLocation}\\Small.test",
                    SmallTestFileSizeInBytes).FullName;

            }
        }

        /// <summary>
        /// Gets the default test files directory location.
        /// </summary>
        /// <value>
        /// The default test files directory location.
        /// </value>
        public string DefaultTestFilesDirectoryLocation => $@"{Directory.GetCurrentDirectory()}\\TestFiles";

        /// <summary>
        /// Gets the large test file location.
        /// </summary>
        /// <value>
        /// The large test file location.
        /// </value>
        public string LargeTestFileLocation
        {
            get
            {
                if (_config.AppSettings.Settings["LargeTestFileLocation"] != null
                    && !string.IsNullOrWhiteSpace(_config.AppSettings.Settings["LargeTestFileLocation"].Value))

                    return _config.AppSettings.Settings["LargeTestFileLocation"].Value;

                if (Directory.Exists(DefaultTestFilesDirectoryLocation))
                {
                    // If the test file exists return the file path, otherwise generate a dump file with 1 GB size
                    return File.Exists($@"{DefaultTestFilesDirectoryLocation}\\Large.test")
                        ? $@"{DefaultTestFilesDirectoryLocation}\\Large.test"
                        : Utilities.Instance.GenerateDumpFile($@"{DefaultTestFilesDirectoryLocation}\\Large.test",
                            LargeTestFileSizeInBytes).FullName;
                }

                Directory.CreateDirectory(DefaultTestFilesDirectoryLocation);
                return Utilities.Instance.GenerateDumpFile($@"{DefaultTestFilesDirectoryLocation}\\Large.test",
                    LargeTestFileSizeInBytes).FullName;
            }
        }

        /// <summary>
        /// Gets the BufferSize value to be used during the copy operation.
        /// </summary>
        /// <value>
        /// The large test file location.
        /// </value>
        public int BufferSizeInBytes => (_config.AppSettings.Settings["BufferSizeInBytes"] != null
                                                && !string.IsNullOrWhiteSpace(_config.AppSettings.Settings["BufferSizeInBytes"].Value))
            ? int.Parse(_config.AppSettings.Settings["BufferSizeInBytes"].Value)
            : 4096;

        /// <summary>
        /// Gets the large test file size in bytes.
        /// </summary>
        /// <value>
        /// The large test file size in bytes.
        /// </value>
        public long LargeTestFileSizeInBytes
        {
            get
            {
                var configValue = _config.AppSettings.Settings["LargeTestFileSizeInBytes"];


                if (configValue != null
                    && !string.IsNullOrWhiteSpace(configValue.Value)
                    && long.TryParse(configValue.ToString(), out var fileSize))
                    return fileSize;

                return 1024 * 1024 * 1024;

            }
        }

        public long SmallTestFileSizeInBytes => (_config.AppSettings.Settings["SmallTestFileSizeInBytes"] != null
                                                 && !string.IsNullOrWhiteSpace(_config.AppSettings.Settings["SmallTestFileSizeInBytes"].Value))
            ? int.Parse(_config.AppSettings.Settings["SmallTestFileSizeInBytes"].Value)
            : 10 * 1024;

        /// <summary>
        /// Prevents a default instance of the <see cref="AppConfigurations" /> class from being created.
        /// </summary>
        private AppConfigurations()
        {

        }

    }
}
