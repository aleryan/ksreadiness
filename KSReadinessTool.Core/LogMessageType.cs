﻿namespace KSReadinessTool.Core
{
    /// <summary>
    /// Enum about the type of the message to be logged to the log file
    /// </summary>
    public enum LogMessageType
    {
        /// <summary>
        /// Error message
        /// </summary>
        Error,

        /// <summary>
        /// Information message
        /// </summary>
        Info,

        /// <summary>
        /// Warning message
        /// </summary>
        Warning
    }
}