﻿namespace KSReadinessTool.Core
{
    /// <summary>
    /// An enum of the different network connection resource types
    /// </summary>
    public enum ResourceType : int
    {
        Any = 0,
        Disk = 1,
        Print = 2,
        Reserved = 8,
    }

}
