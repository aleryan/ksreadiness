using System;
using System.IO;
using System.Net;

namespace KSReadinessTool.Core
{
	/// <summary>
	/// Class to manage generic functions
	/// </summary>
	public class Utilities
	{
		/// <summary>
		/// App Logs directory Path
		/// </summary>
		private readonly string _logDirectoryPath = $@"{Directory.GetCurrentDirectory()}\\Logs";

		/// <summary>
		/// Singleton Instance object
		/// </summary>
		public static readonly Utilities Instance = new Utilities();

		/// <summary>
		/// Prevents a default instance of the <see cref="Utilities"/> class from being created.
		/// </summary>
		private Utilities()
		{

		}

		/// <summary>
		/// Writes a srtring to log file.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <param name="type">The type.</param>
		public void WriteToLog(string message, LogMessageType type)
		{
			if (!Directory.Exists(_logDirectoryPath))
			{
				Directory.CreateDirectory(_logDirectoryPath);
			}

			var logFileName = $@"{_logDirectoryPath}\\{DateTime.Now.Year}{DateTime.Now.Month:D2}{DateTime.Now.Day:D2}_KSR.log";

			File.AppendAllText(logFileName, $@"{DateTime.Now}: {type}  {message}");
			File.AppendAllText(logFileName, Environment.NewLine);
		}

		/// <summary>
		/// Get the Public IP Address
		/// </summary>
		/// <returns></returns>
		public string GetPublicIpAddress()
		{
			try
			{
				var responseFromServer = "";
				var request = WebRequest.Create("https://api.ipify.org");

				var response = request.GetResponse();
				using (var dataStream = response.GetResponseStream())
				{
					// Open the stream using a StreamReader for easy access.  
					var reader = new StreamReader(dataStream);
					// Read the content.  
					responseFromServer = reader.ReadToEnd();
					// Display the content.  

				}

				return responseFromServer;
			}
			catch (Exception exception)
			{
				Utilities.Instance.WriteToLog(exception.ToString(), LogMessageType.Error);
				return "";

			}
		}

		/// <summary>
		/// Formats the size of the byte.
		/// </summary>
		/// <param name="bytes">The bytes.</param>
		/// <returns></returns>
		public string FormatByteSize(double bytes)
		{
			string[] suffix = { "B", "KB", "MB", "GB", "TB" };

			int index = 0;

			do
			{
				bytes /= 1024;
				index++;
			} while (bytes >= 1024);

			return $"{bytes:0.00} {suffix[index]}";
		}

		/// <summary>
		/// Formats the byte size to Mb format.
		/// </summary>
		/// <param name="bytes">The bytes.</param>
		/// <returns></returns>
		public double FormatByteSizeToMb(double bytes)
		{
			return Math.Round((bytes / 1024) / 1024, 2);


		}

        /// <summary>
        /// Generates a dump file with a specific size.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="sizeInBytes">The size in bytes.</param>
        /// <returns></returns>
        public FileInfo GenerateDumpFile(string path, long sizeInBytes)
        {

            byte[] data = sizeInBytes < 20960 ? new byte[500] : new byte[20960];

            var rng = new Random();

            var loops = (int) (sizeInBytes / data.Length);

            using (var stream = File.OpenWrite(path))
            {
                for (var i = 0; i < loops; i++)
                {
                    rng.NextBytes(data);
                    stream.Write(data, 0, data.Length);
                }
            }
            return new FileInfo(path);
        }
    }
}
