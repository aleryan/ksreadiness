using System;
using System.Collections.Generic;
using System.Linq;

namespace KSReadinessTool.Core
{
	/// <summary>
	/// A class to hold the attributes of the Test Result object
	/// </summary>
	public class TestResult
	{
		/// <summary>
		/// Generate the Test Time text
		/// </summary>
		/// <value>
		/// The test time.
		/// </value>
		public string TestTime =>
			$@"{DateTime.Now.Year}{DateTime.Now.Month:D2}{DateTime.Now.Day:D2}_{DateTime.Now.Hour:D2}{DateTime.Now.Minute:D2}{DateTime.Now.Second:D2}";

		/// <summary>
		/// Gets or sets the name of the file.
		/// </summary>
		/// <value>
		/// The name of the file.
		/// </value>
		public string FileName { get; set; }

		/// <summary>
		/// Gets or sets the size of the file.
		/// </summary>
		/// <value>
		/// The size of the file.
		/// </value>
		public long FileSize { get; set; }

		/// <summary>
		/// Gets or sets the start time.
		/// </summary>
		/// <value>
		/// The start time.
		/// </value>
		public DateTime StartTime { get; set; }

		/// <summary>
		/// Gets or sets the end time.
		/// </summary>
		/// <value>
		/// The end time.
		/// </value>
		public DateTime EndTime { get; set; }

		/// <summary>
		/// Gets the total transfer time. Auto generated value in Seconds (End time - Start time)
		/// </summary>
		/// <value>
		/// The total transfer time.
		/// </value>
		public int TotalTransferTime
		{
			get
			{
				var timeSpan = EndTime.Subtract(StartTime);
				return (int)timeSpan.TotalSeconds;
			}
		}

		/// <summary>
		/// Gets or sets the transfer rate per sec collection.
		/// </summary>
		/// <value>
		/// The transfer rate per sec.
		/// </value>
		public List<int> TransferRatesPerSec
		{
			get; set;
		}

		/// <summary>
		/// Average trasfer rate per sec for the current running job
		/// </summary>
		public double AverageTransferRatePerSec
		{
			get { return TransferRatesPerSec != null && TransferRatesPerSec.Any(r => r > 0) ? TransferRatesPerSec.Average() : 0; }
		}

		/// <summary>
		/// Gets or sets the most recent transfer rate per second.
		/// </summary>
		/// <value>
		/// The transfer rate per second.
		/// </value>
		public int RecentTransferRatePerSecond
		{
			get; set;
		}

		/// <summary>
		/// Total Size of the file being transferred
		/// </summary>
		public long TotalContentSize
		{
			get; set;
		}

		/// <summary>
		/// The Size of the processed content from the over all file content size
		/// </summary>
		public long ProcessedContentSize
		{
			get; set;
		}

		/// <summary>
		/// Gets or sets the Test Status.
		/// </summary>
		/// <value>
		/// The status.
		/// </value>
		public TestStatus Status
		{
			get; set;
		}

	}
}
