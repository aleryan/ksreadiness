namespace KSReadinessTool.Core
{
    /// <summary>
    /// Test Transfer Status
    /// </summary>
    public enum TestStatus
    {
        ErrorOccured = -5,
        Starting = -4,
        TestingConnectivityToDestination = -3,
        TestingConnectivitySucceeded = -2,
        TransferInProgress = -11,
        TransferCompleted = -12,
        ClosingConnection = -50,
        TestTransferCompletedSuccessfully = 100,
    }
}
